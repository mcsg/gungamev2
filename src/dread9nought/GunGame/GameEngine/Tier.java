package dread9nought.GunGame.GameEngine;

import org.bukkit.inventory.ItemStack;

public class Tier
{
	Integer TierID = 0;
	ItemStack Head;
	ItemStack Chest;
	ItemStack Legs;
	ItemStack Boots;
	ItemStack[] Items;
	
	public Tier(){}
	
	public Integer getID() { 
		return TierID;
	}
	public void setID(Integer id) {
		TierID = id;
	}
	public ItemStack getHead() {
		return Head;
	}
	public void setHead(ItemStack item) {
		Head = item;
	}
	public ItemStack getChest() {
		return Chest;
	}
	public void setChest(ItemStack item) {
		Chest = item;
	}
	public ItemStack getLegs() {
		return Legs;
	}
	public void setLegs(ItemStack item) {
		Legs = item;
	}
	public ItemStack getBoots() {
		return Boots;
	}
	public void setBoots(ItemStack item) {
		Boots = item;
	}
	public ItemStack[] getItems() {
		return Items;
	}
	public void setItems(ItemStack[] array) {
		Items = array;
	}
}
