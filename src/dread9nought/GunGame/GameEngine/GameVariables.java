package dread9nought.GunGame.GameEngine;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import dread9nought.GunGame.GPlayer;
import dread9nought.GunGame.GameEngine.GameTasks.TaskVariables;

public class GameVariables extends TaskVariables
{
	private static Game CurrentGame;
	private static Map CurrentMap;
	
	private static List<GPlayer> Players = new ArrayList<GPlayer>();
	
	private static Integer maxPlayers = 24;
	private static Integer minPlayers = 2;
	private static Integer lobbyTime = 5;
	private static Integer lobbyShortTime = 15;
	private static Integer gameTime = 600;
	private static Integer lastSpawn = 0;
	private static Integer heighestTier = 0;
	protected static String Prefix = ChatColor.DARK_AQUA+"[GunGame] ";
	
	public static Integer getMaxPlayers() {
		return maxPlayers;
	}
	public static void setMaxPlayers(Integer value) {
		maxPlayers = value;
	}
	public static Integer getMinPlayers() {
		return minPlayers;
	}
	public static void setMinPlayers(Integer value) {
		minPlayers = value;
	}
	public static Integer getLobbyTime() {
		return lobbyTime;
	}
	public static void setLobbyTime(Integer value) {
		lobbyTime = value;
	}
	public static Integer getLobbyShortTime() {
		return lobbyShortTime;
	}
	public static void setLobbyShortTime(Integer value) {
		lobbyShortTime = value;
	}
	public static Integer getGameTime() {
		return gameTime;
	}
	public static void setGameTime(Integer value) {
		gameTime = value;
	}
	public static Integer getLastSpawn() {
		return lastSpawn;
	}
	public static void setLastSpawn(Integer value) {
		lastSpawn = value;
	}
	public static Integer getHeighestTier() {
		return heighestTier;
	}
	public static void setHeighestTier(Integer value) {
		heighestTier = value;
	}
	public static String getPrefix() {
		return Prefix;
	}
	public static List<GPlayer> getPlayers() {
		return Players;
	}
	public static Map getMap() {
		return CurrentMap;
	}
	public static Map createMap() {
		CurrentMap = new Map();
		return CurrentMap;
	}
	public static Game getGame() {
		return CurrentGame;
	}
	public static Game createGame() {
		CurrentGame = new Game();
		return CurrentGame;
	}
	
	
	//Player methods
	public static GPlayer getPlayer(String name) {
		for(GPlayer o : getPlayers().toArray(new GPlayer[getPlayers().size()])) {
			if (o.getName().equalsIgnoreCase(name))
				return o;
		}
		return null;
	}
	public static GPlayer getPlayer(Player p) {
		for(GPlayer o : getPlayers().toArray(new GPlayer[getPlayers().size()])) {
			if (o.getName().equalsIgnoreCase(p.getName()))
				return o;
		}
		return null;
	}
	public static boolean containsPlayer(GPlayer player) {
		return getPlayer(player.getName()) != null;
	}
	public static boolean containsPlayer(Player player) {
		return getPlayer(player.getName()) != null;
	}
	public static void addPlayer(GPlayer g) {
		getPlayers().add(g);
	}
}

