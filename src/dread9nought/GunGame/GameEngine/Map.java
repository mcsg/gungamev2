package dread9nought.GunGame.GameEngine;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

public class Map
{
	private String Name;
	private List<Location> SpawnPoints;
	private Location Lobby;
	private int lastSpawn;
	
	public Map()
	{
		Name = "";
		SpawnPoints = new ArrayList<Location>();
	}
	
	public Integer addSpawn(Location loc) {
		SpawnPoints.add(loc);
		return SpawnPoints.size();
	}
	public Location getSpawn(Integer loc) {
		Location rtn = null;
		
		if (SpawnPoints.size() >= (loc-1)) {
			rtn = (Location)SpawnPoints.toArray()[loc-1];
		}
		return rtn;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getName() {
		return Name;
	}
	public void setLobby(Location loc) {
		Lobby = loc;
	}
	public Location getLobby() {
		return Lobby;
	}
	public Location getNextSpawn() {
		if (lastSpawn == this.SpawnPoints.size()) { lastSpawn = 0; }
		
		Location rtn = SpawnPoints.get(lastSpawn);
		lastSpawn++;
		return rtn;
	}
}
