package dread9nought.GunGame.GameEngine;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

public class Events implements Listener
{
	@EventHandler(priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent ev) {
		Engine.addPlayer(ev.getPlayer());
	}
}
