package dread9nought.GunGame.GameEngine;

import org.bukkit.entity.Player;

public class Engine
{
	public static void Prep() {
		GameVariables.createMap();
		GameVariables.createGame();
	}
	public static void addPlayer(Player p) {
		GameVariables.getGame().signUp(p);
	}
}
