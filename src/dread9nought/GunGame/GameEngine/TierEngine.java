package dread9nought.GunGame.GameEngine;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import dread9nought.GunGame.GPlayer;
import dread9nought.GunGame.GunGame;

public class TierEngine
{
	private static HashMap<Integer, Tier> Tiers = new HashMap<Integer, Tier>();
	
	public static Tier getTier(Integer ID) {
		//y u no haz linq java zz
		for(Tier tier : Tiers.values()) {
			if (tier.TierID == ID)
				return tier;
		}
		return null;
	}
	public static void addTier(Tier tier)
	{
		if (!Tiers.containsKey(tier.TierID))
			Tiers.put(tier.TierID, tier);
		else
			GunGame.getPlugin().getLogger().severe("Tier already exists! Tier ID: "+ tier.TierID);
	}
	@SuppressWarnings("deprecation")
	public static void giveTier(GPlayer gPlayer, Tier tier)
	{
		//Micro optimization - Temp store player, rather than finding and grabbing it every single time
		Player player = gPlayer.toPlayer();
		
		clearInventory(player);//Don't send GPlayer, another MO - No point finding the player again, when we already have it
		
		player.getInventory().setHelmet(tier.Head);
		player.getInventory().setChestplate(tier.Chest);
		player.getInventory().setLeggings(tier.Legs);
		player.getInventory().setBoots(tier.Boots);
		for(int i =0;i<tier.Items.length;i++) { player.getInventory().addItem(tier.Items[i]); }
		player.updateInventory();
	}
	public static void clearInventory(GPlayer gPlayer) {
		clearInventory(gPlayer.toPlayer());
	}
	@SuppressWarnings("deprecation")
	public static void clearInventory(Player player) {
		player.getInventory().setArmorContents(new ItemStack[]{});
		player.getInventory().setContents(new ItemStack[]{});
		player.updateInventory();
	}

	public static Integer getTierSize() {
		return Tiers.size();
	}

	public static Integer getTierLevel(GPlayer p) {
		return p.getLevel();
	}
	public static void increaseTierLevel(GPlayer p) {
		p.setLevel((p.getLevel())+1);
		giveTier(p, getTier(p.getLevel()));
	}
	public static void descreaseTierLevel(GPlayer p) {
		if (p.getLevel() == 0) { return; }
		
		p.setLevel((p.getLevel())-1);
		
		giveTier(p, getTier(p.getLevel()));
	}
}
