package dread9nought.GunGame.GameEngine.GameTasks;

import java.util.Arrays;

import org.bukkit.entity.Player;

import dread9nought.GunGame.GPlayer;
import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.GameEngine.GameVariables;
import dread9nought.GunGame.GameEngine.Game.StartSequence;

public class TeleportTask
{
	int TaskID;
	int lastindex;
	GPlayer[] players;
	
	public TeleportTask() {
		lastindex = 0;
		players = new GPlayer[GameVariables.getPlayers().size()];
		System.arraycopy(GameVariables.getPlayers().toArray(new GPlayer[GameVariables.getPlayers().size()]), 0, players, 0, GameVariables.getPlayers().size());
	}
	
	public void Start()
	{
		TaskID = GunGame.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(GunGame.getPlugin(), new Runnable()
		{
			public void run()
			{
				if (lastindex == players.length) { Stop(); return; }
				
				Player p = players[lastindex].toPlayer();
				if (p != null) {
					p.teleport(GameVariables.getMap().getNextSpawn());
				}
				lastindex++;
			}
		}, 0L, 2L);
	}
	public void Stop()
	{
		GunGame.getPlugin().getServer().getScheduler().cancelTask(TaskID);
		empty();
		GameVariables.getGame().initiatePhase(StartSequence.PlayBall);
	}
	private void empty() {
		Arrays.fill(players, null);
		players = new GPlayer[0];
	}
}
