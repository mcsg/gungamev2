package dread9nought.GunGame.GameEngine.GameTasks;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import dread9nought.GunGame.GPlayer;
import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.GameEngine.GameVariables;
import dread9nought.GunGame.GameEngine.Game.StartSequence;

public class LobbyTask
{
	int TaskID;
	boolean fast = false;
	
	public LobbyTask(boolean _fast) {
		fast = _fast;
	}
	
	public void Start()
	{
		TaskID = GunGame.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(GunGame.getPlugin(), new Runnable()
		{
			Integer counter = (fast ? GameVariables.getLobbyShortTime() : GameVariables.getLobbyTime());
			Integer counter2 = 30;
			public void run()
			{
				GameVariables.getGame().setAllEXP(counter);
				if (counter == 0) {
					GameVariables.getGame().initiatePhase(StartSequence.PREPARE);
					Stop();
					return;
				}
				if (counter2 == 0) {
					counter2 = 15;
					Random r = new Random();
					int i = r.nextInt(2);
					switch(i)
					{
						case 0:
							GameVariables.getGame().sendMessageToGame(ChatColor.YELLOW + "The following map will be played: "+ChatColor.AQUA+GameVariables.getMap().getName(), true);
							break;
						case 1:
							GameVariables.getGame().sendMessageToGame(ChatColor.YELLOW + "There are: "+ChatColor.AQUA+GameVariables.getGame().getPlayerCount() + ChatColor.YELLOW + " players waiting to play", true);
							break;
						case 2:
							GameVariables.getGame().sendMessageToGame(ChatColor.YELLOW + "The game will start in: "+ChatColor.AQUA+counter+ChatColor.YELLOW+" seconds", true);
							break;
					}
				}
				for(GPlayer g : GameVariables.getPlayers().toArray(new GPlayer[GameVariables.getPlayers().size()]))
				{
					Player player = g.toPlayer();
					if (player != null) {
						if (!player.getWorld().getName().equalsIgnoreCase(GameVariables.getMap().getLobby().getWorld().getName()))
							player.teleport(GameVariables.getMap().getLobby());
					}
				}
				counter--;
				counter2--;
			}
		}, 0L, 20L);
	}
	public void Stop()
	{
		GunGame.getPlugin().getServer().getScheduler().cancelTask(TaskID);
	}
}
