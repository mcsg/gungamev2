package dread9nought.GunGame.GameEngine.GameTasks;

public class TaskVariables
{
	private static LobbyTask lobbyTask;
	private static TeleportTask teleportTask;
	
	public static TeleportTask getTeleportTask() {
		return teleportTask;
	}
	public static boolean teleportTaskStarted() {
		return teleportTask == null;
	}
	public static void startTeleportTask() {
		teleportTask = new TeleportTask();
	}
	public static void stopTeleportTask() {
		if (teleportTaskStarted()) {
			teleportTask.Stop();
		}
	}
	public static LobbyTask getLobbyTask() {
		return lobbyTask;
	}
	public static boolean lobbyTaskStarted() {
		return lobbyTask == null;
	}
	public static void startLobbyTask(boolean fast) {
		lobbyTask = new LobbyTask(fast);
	}
	public static void stopLobbyTask() {
		if (lobbyTaskStarted())
			lobbyTask.Stop();
	}
}
