package dread9nought.GunGame.GameEngine;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import dread9nought.GunGame.GPlayer;
import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.ServerInteraction.InteractType;
import dread9nought.GunGame.ServerInteraction.ServerInteractor;

public class Game extends GameVariables
{
	public enum GameStage { WAITING(1), INGAME(2); int foo = 0; GameStage(int boo) { foo = boo; } }
	public enum StartSequence { WAITING(1), PREPARE(2), TELEPORT(3), PlayBall(3), AnnounceWinner(4), StartDeprep(5); int foo = 0; StartSequence(int boo) { foo = boo; } }
	
	private GameStage Status = GameStage.WAITING;
	private StartSequence Phase;
	private StartSequence previousPhase;
	
	public Game() { }
	
	public void signUp(Player player) {
		if (Full())
		{
			boolean kick = false;
			if (player.hasPermission("gg.joinfull") || player.isOp()) {
				if (kickNonDonator()) {
					hardJoin(player);
				}
				else {
					player.sendMessage(Prefix + ChatColor.GOLD + "The game is full of donators!");
					kick = true;
				}
			}
			else {
				player.sendMessage(Prefix + ChatColor.GOLD + "The game is full");
				kick = true;
			}
			if (kick)
				hardKick(player);
		}
		else if (getStatus() == GameStage.INGAME)
		{
			boolean kick = false;
			if (!player.isOp()) {
				player.sendMessage(Prefix + ChatColor.GOLD + "Game in progress");
				kick = true;
			}
			if (!kickNonDonator()) {
				kick = true;
				player.sendMessage(Prefix + ChatColor.GOLD + "The game is full of donators");
			}
			if (kick)
			{
				hardKick(player);
			}
			else
			{
				hardJoin(player);
			}
		}
		else if (getStatus() == GameStage.WAITING)
		{
			hardJoin(player);
		}
	}
	public void hardJoin(Player player)
	{
		GPlayer g = new GPlayer(player.getName());
		addPlayer(g);
		
		if (getStatus() == GameStage.WAITING)
			player.teleport(getMap().getLobby());
		else
			player.teleport(getMap().getNextSpawn());
		
		sendMessageToGame(ChatColor.GOLD + g.getName() + ChatColor.YELLOW + " has joined the game", true);
		ServerInteractor.notifyServer(InteractType.UPDATE);
		
		initiatePhase(StartSequence.WAITING);
	}
	public boolean kickNonDonator()
	{
		for (GPlayer g : getPlayers()) {
			if (g.toPlayer().hasPermission("gg.joinfull")) {
				Leave(g);
				return true;
			}
		}
		return false;
	}
	
	public void Leave(GPlayer g) {
		GunGame.getPlugin().getLilypad().redirect(GunGame.getPlugin().getLilypad().hub, g.toPlayer());
		getPlayers().remove(g);
	}
	public void Leave(String name) {
		GPlayer g = getPlayer(name);
		if (g != null) {
			Leave(g);
		}
	}
	public void Leave(Player player) {
		GPlayer g = getPlayer(player);
		if (g != null) {
			Leave(g);
		}
	}
	private void hardKick(Player player) {
		GunGame.getPlugin().getLilypad().redirect(GunGame.getPlugin().getLilypad().hub, player);
	}
	
	public void startShutdown()
	{
		
	}
	
	//Core methods (initiatePhase, signup, killed etc)
	public void initiatePhase(StartSequence phase) {
		if (phase != null)
		{
			GunGame.getPlugin().getLogger().info("Target phase foo: "+phase.foo + " current phase foo: "+getPhase().foo);
			if (getPhase().foo >= phase.foo) { return; }
		}
		if (getPhase() != null) {
			setPreviousPhase(getPhase());
		}
		setPhase(phase);
		GunGame.getPlugin().getLogger().info("Game phase initiated: "+phase.toString());
		switch(phase)
		{
			case WAITING:
			{
				if (!lobbyTaskStarted()) {
					startLobbyTask(false);//set to true if timer should go qweek
				}
				break;
			}
			case PREPARE:
			{
				if (getPreviousPhase() != StartSequence.WAITING) { return; }
				
				if (getPlayerCount() < getMinPlayers()) {
					for(GPlayer g : getPlayers())
						g.toPlayer().sendMessage(Prefix + ChatColor.RED + "Not enough players to start! :(");
					initiatePhase(StartSequence.StartDeprep);
					return;
				}
				
				//First phase that is called, while its order does not matter
				//This will always be the first phase in which the game begins
				//And therefor we set the game status to INGAME from here on out
				setStatus(GameStage.INGAME);
				//M
				for(GPlayer g : getPlayers().toArray(new GPlayer[getPlayerCount()])) {
					TierEngine.giveTier(g, TierEngine.getTier(g.getLevel()));
				}
				initiatePhase(StartSequence.TELEPORT);
				break;
			}
			case TELEPORT:
			{
				if (getPreviousPhase() != StartSequence.PREPARE) { return; }
				if (!teleportTaskStarted()) {
					startTeleportTask();
				}
				//Start the teleport task, teleporting 1 player per 2 ticks to prevent any chance of a
				//Same-tick invisible glitch happening
				//PlayBall phase initiated by the end of the teleport task, called from itself
				break;
			}
			case PlayBall:
			{
				if (getPreviousPhase() != StartSequence.TELEPORT) { return; }
				//Allow the game timer to start
				//AnnounceWinner called by the killed() code when a winner is due
				break;
			}
			case AnnounceWinner:
			{
				if (getPreviousPhase() != StartSequence.PlayBall) { return; }
				setStatus(GameStage.WAITING);
				//Stop the game timer
				//Disable pvp
				//Disable projectiles
				//Do the funky scoreboard
				//Do the fireworks
				//StartDeprep phase is initiated via a task timer
				//Which should be done here
				break;
			}
			case StartDeprep:
			{
				//Kicks all players to hub
				//Shuts server down
				if (getPreviousPhase() != StartSequence.AnnounceWinner) { return; }
				ServerInteractor.notifyServer(InteractType.SHUTDOWN);
				GunGame.getPlugin().setReadyForUse(false);
				
				for(GPlayer g : getPlayers().toArray(new GPlayer[getPlayerCount()])) {
					hardKick(g.toPlayer());
				}
				break;
			}
			default:
				break;
		}
	}

	
	//Sub methods (ie sendMessageToGame, sendSound, setEXP etc)
	public void sendMessageToGame(String msg, boolean prefix) {
		String tosend = msg;
		if (prefix) {
			tosend = getPrefix()+msg;
		}
		for(GPlayer g : getPlayers()) {
			g.toPlayer().sendMessage(tosend);
		}
	}
	public void sendSound(Sound sound) {
		for(GPlayer g : getPlayers()) {
			//Store player, 0.0000001ms faster
			Player p = g.toPlayer();
			p.playSound(p.getLocation(), sound, 100, 1);
		}
	}
	public void setAllEXP(Integer level) {
		for (GPlayer g : getPlayers()) {
			g.toPlayer().setLevel(level);
		}
	}
	
	
	//Get/set accessors
	public GameStage getStatus() {
		return Status;
	}
	public void setStatus(GameStage stage) {
		Status = stage;
		ServerInteractor.notifyServer(InteractType.UPDATE);
	}
	public StartSequence getPhase() {
		return Phase;
	}
	private void setPhase(StartSequence phase) {
		Phase = phase;
		ServerInteractor.notifyServer(InteractType.UPDATE);
	}
	public StartSequence getPreviousPhase() {
		return previousPhase;
	}
	private void setPreviousPhase(StartSequence phase) {
		previousPhase = phase;
	}
	
	public Integer getPlayerCount() {
		return getPlayers().size();
	}
	public boolean Full() { 
		return getPlayerCount() >= getMaxPlayers();
	}
}
