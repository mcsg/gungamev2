package dread9nought.GunGame.ServerInteraction;

import org.bukkit.ChatColor;

import Networking.Packets.SignUpdatePacket;

import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.GameEngine.GameVariables;
import dread9nought.Main.ClientStuff;

public class ServerInteractor
{
	//Amount of lines on a sign, currently 4
	static int signLineAmount = 4;
	
	public static void notifyServer(InteractType type) {
		switch(type)
		{
			case UPDATE:
				standardUpdate();
				break;
			case SHUTDOWN:
				shutdownUpdate();
				break;
		}
	}
	static void standardUpdate() {
		String[] lines = new String[getLineAmount()];
		switch(GameVariables.getGame().getStatus()) {
			case WAITING:
				lines[0] = ChatColor.GREEN + "[JOIN]";
				break;
			case INGAME:
				lines[0] = ChatColor.RED + "[INGAME]";
				break;
		}
		if (GameVariables.getGame().Full())
			lines[0] = ChatColor.RED + "[FULL]";
		
		lines[1] = ChatColor.YELLOW+GameVariables.getMap().getName();
		lines[2] = ChatColor.DARK_PURPLE+GunGame.getPlugin().getServerName();
		lines[3] = ChatColor.BLUE+""+GameVariables.getGame().getPlayerCount()+"/"+GameVariables.getMaxPlayers();
		
		//Static class in DreadAPI
		ClientStuff.SendPacketToServers(constructPacket(lines));
	}
	static void shutdownUpdate() {
		String[] lines = new String[getLineAmount()];
		lines[0] = ChatColor.RED+""+ChatColor.BOLD+"RESTARTING";
		lines[1] = ChatColor.GREEN + GunGame.getPlugin().getServerName();
		lines[2] = "";
		lines[3] = lines[0];
		
		//Static class in DreadAPI
		ClientStuff.SendPacketToServers(constructPacket(lines));
	}
	
	static byte[] constructPacket(String[] data) {
		SignUpdatePacket pkt = new SignUpdatePacket();
		pkt.id = 0x01;
		pkt.line1 = data[0];
		pkt.line2 = data[1];
		pkt.line3 = data[2];
		pkt.line4 = data[3];
		pkt.server = GunGame.getPlugin().getServerName();
		
		return pkt.Build();
	}
	
	static int getLineAmount() {
		return signLineAmount;
	}
}
