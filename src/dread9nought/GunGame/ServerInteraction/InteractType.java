package dread9nought.GunGame.ServerInteraction;

public enum InteractType {
	UPDATE(1),
	SHUTDOWN(2);
	
	public int stage;
	InteractType(int s) {
		stage  = s;
	}
}
