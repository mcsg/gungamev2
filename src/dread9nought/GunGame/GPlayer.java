package dread9nought.GunGame;

import org.bukkit.entity.Player;

public class GPlayer
{
	//No direct input
	private String name;
	private Integer level = 0;
	
	public GPlayer(String name) {
		this.setName(name);
	}
	
	public String getName() {
		return name;
	}
	
	private void setName(String s) {
		name = s;
	}
	
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer value) {
		level = value;
	}
	
	public Player toPlayer() {
		return GunGame.getPlugin().getServer().getPlayerExact(this.getName());
	}
}
