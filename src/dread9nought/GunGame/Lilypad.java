package dread9nought.GunGame;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.request.impl.RedirectRequest;
import lilypad.client.connect.api.result.FutureResultListener;
import lilypad.client.connect.api.result.StatusCode;
import lilypad.client.connect.api.result.impl.RedirectResult;


public class Lilypad 
{
	Connect connect;
	public String hub = "gungame";
	public List<String> Players = new ArrayList<String>();
	public Lilypad()
	{
		connect = GunGame.getPlugin().getServer().getServicesManager().getRegistration(Connect.class).getProvider();
	}
	public void redirect(String server, final Player player)
	{
	    try
	    {
	    	if (this.connect == null || this.connect.isClosed()) { connect = GunGame.getPlugin().getServer().getServicesManager().getRegistration(Connect.class).getProvider(); }
		    
	    	if (this.connect.isConnected()) {
		    	this.connect.request(new RedirectRequest(server, player.getName())).registerListener(new FutureResultListener<RedirectResult>()
			    {	
			    	public void onResult(RedirectResult redirectResult) {
			    		if (redirectResult.getStatusCode() == StatusCode.SUCCESS) {
			    			return;
			    		}
			        	player.sendMessage(ChatColor.RED + "Connection error");
			        }
			    });
		    }
		    else
		    	player.sendMessage(ChatColor.RED+"[ERROR] Connection down");
	 }
	 catch (Exception exception) {
	     player.sendMessage(ChatColor.RED + "Connection exception");
	 }
	}
}
