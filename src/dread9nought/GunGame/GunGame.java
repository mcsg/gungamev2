package dread9nought.GunGame;

import org.bukkit.plugin.java.JavaPlugin;

import dread9nought.GunGame.GameEngine.Engine;
import dread9nought.GunGame.GameEngine.Events;
import dread9nought.GunGame.ServerInteraction.InteractType;
import dread9nought.GunGame.ServerInteraction.ServerInteractor;
import dread9nought.Main.DreadAPI;
import dread9nought.Main.StaticWorldSystem;
import dread9nought.Main.Updater;

public class GunGame extends JavaPlugin
{
	//Static variables
	private static GunGame instance;
	
	//Data type variables (that are not an object!)
	private boolean readyForPlaying = false;
	private String serverName;
	
	//Object type variables (classes for instance)
	private DreadAPI dreadAPI;
	private Updater update;
	private StaticWorldSystem worldSystem;
	private StaticWorldSystem lobbyWorldSystem;
	private Lilypad lilyPad;
	
	@Override
	public void onEnable() {
		setPluginInstance(this);
		setDreadAPI(DreadAPI.Core);
		setServerName(this.getServer().getServerName());
		setUpdater(getDreadAPI().getUpdater(this));
		
		//Try catch for updater (throws IOException)
		try
		{
			if (getUpdater().outOfDate()) {
				getUpdater().update();
				ServerInteractor.notifyServer(InteractType.SHUTDOWN);
			}
			else
			{
				//Load gameWorld
				setGameWorldSystem(getDreadAPI().getStaticWorldSystem());
				String gameWorld = StaticWorldSystem.DownloadRandomWorld();
				getGameWorldSystem().LoadWorld(gameWorld);
				
				//Load lobbyWorld
				setLobbyWorldSystem(getDreadAPI().getStaticWorldSystem());
				getLobbyWorldSystem().LoadWorld("LobbyWorld");
				
				//Prep game & map instances
				Engine.Prep();
				
				//Events
				this.getServer().getPluginManager().registerEvents(new Events(), this);
				
				//Tell the hub we're good to go.
				ServerInteractor.notifyServer(InteractType.UPDATE);
			}
		}
		catch(Exception e) { e.printStackTrace(); }
	}
	@Override
	public void onDisable()
	{
		//throws IOException
		try
		{
			if (!getUpdater().outOfDate()) {
				//Unload and delete worlds
				getGameWorldSystem().DeleteWorld();
				getLobbyWorldSystem().DeleteWorld();
			}
		}
		catch(Exception e) { e.printStackTrace(); }
	}
	
	
	//get/set accessors woooo oh so many
	public static GunGame getPlugin() {
		return instance;
	}
	public static void setPluginInstance(GunGame inst) {
		instance = inst;
	}
	public boolean getReadyForUse() {
		return readyForPlaying;
	}
	public void setReadyForUse(boolean value) {
		readyForPlaying = value;
	}
	public DreadAPI getDreadAPI() {
		return dreadAPI;
	}
	public void setDreadAPI(DreadAPI api) {
		dreadAPI = api;
	}
	public StaticWorldSystem getGameWorldSystem() {
		return worldSystem;
	}
	public void setGameWorldSystem(StaticWorldSystem system) {
		worldSystem = system;
	}
	public StaticWorldSystem getLobbyWorldSystem() {
		return lobbyWorldSystem;
	}
	public void setLobbyWorldSystem(StaticWorldSystem system) {
		lobbyWorldSystem = system;
	}
	public Lilypad getLilypad() {
		return lilyPad;
	}
	public void setLilypad(Lilypad lily) {
		lilyPad = lily;
	}
	public Updater getUpdater() {
		return update;
	}
	public void setUpdater(Updater updater) {
		update = updater;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String name) { 
		serverName = name;
	}
}
