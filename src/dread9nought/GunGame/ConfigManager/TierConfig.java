package dread9nought.GunGame.ConfigManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.GameEngine.GameVariables;
import dread9nought.GunGame.GameEngine.Tier;
import dread9nought.GunGame.GameEngine.TierEngine;

public class TierConfig 
{
	public TierConfig() { Load(); }
	
	YamlConfiguration config;
	String mainPath = GunGame.getPlugin().getDataFolder() + "/";
	String path = mainPath + "tierConfig.yml";
	
	public void Load()
	{
		if (!new File(mainPath).exists()) { new File(mainPath).mkdir(); }
		File file = new File(path);
		if (!file.exists()) { try { file.createNewFile(); } catch(IOException e) { e.printStackTrace(); return; } }
		
		config = new YamlConfiguration();
		try { config.load(path); } catch (IOException | InvalidConfigurationException e) { e.printStackTrace(); return; }
		
		//Load
		for (int i =0;i<500;i++) {
			if (config.contains("Tier"+i))
			{
				String header = "Tier"+i+".";
				Tier tier = new Tier();
				tier.setID(i);
				tier.setHead(loadItem(header + "Head."));
				tier.setChest(loadItem(header + "Chest."));
				tier.setLegs(loadItem(header + "Legs."));
				tier.setBoots(loadItem(header + "Boots."));
				tier.setItems(LoadItems(header));
				
				TierEngine.addTier(tier);
			}
			else
				break;
		}
		
		//Set high tier so global variables doesnt always try to use getTierSize(); which due to how hashmaps work
		//Will recalculate it each time, Micro optimization ftw
		GameVariables.setHeighestTier(TierEngine.getTierSize());
	}
	public void AddTier()
	{
		Tier tier = new Tier();
		tier.setBoots(new ItemStack(Material.DIAMOND_BOOTS, 1));
		tier.setChest(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
		tier.getChest().addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		tier.setLegs(new ItemStack(Material.IRON_LEGGINGS, 1));
		tier.setHead(new ItemStack(Material.LEATHER_HELMET, 1));
		ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
		sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		tier.setItems(new ItemStack[] { sword, new ItemStack(Material.BOW, 1), new ItemStack(Material.ARROW, 64) });
		
		String header = "Tier"+TierEngine.getTierSize()+".";
		SaveItem(header + "Head.", tier.getHead());
		SaveItem(header + "Chest.", tier.getChest());
		SaveItem(header + "Legs.", tier.getLegs());
		SaveItem(header + "Boots.", tier.getBoots());
		config.set(header + "ItemCount", tier.getItems().length);
		for (int i =0;i<tier.getItems().length;i++) {
			SaveItem(header + "Item"+i+".", tier.getItems()[i]);
		}
		try { config.save(path); } catch (IOException e) { e.printStackTrace(); return; }
	}
	public ItemStack[] LoadItems(String field)
	{
		field = field + ".";
		Integer count = config.getInt(field + "ItemCount");
		ItemStack[] Items = new ItemStack[count];
		for (int i =0;i<count;i++) {
			String header = field + "Item"+i+".";
			Items[i] = loadItem(header);
		}
		return Items;
	}
	public ItemStack loadItem(String field) {
		ItemStack item;
		
		Integer size = config.getInt(field + "Size");
		Integer type = config.getInt(field + "ItemTypeID");
		
		if (type == 0) { return new ItemStack(Material.AIR, 1); }
		item = new ItemStack(type, size);
		if (config.contains(field + "ItemData"))
			item = new ItemStack(type, size, (byte)config.getInt(field + "ItemData"));
		if (config.contains(field + "Projectile"))
		{
			String projectile = config.getString(field + "Projectile");
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.DARK_PURPLE+"<<< "+ChatColor.GREEN + projectile + " GUN "+ChatColor.DARK_PURPLE+">>>");
			List<String> l = new ArrayList<String>();
			l.add(projectile);
			meta.setLore(l);
			item.setItemMeta(meta);
		}
		return ReadEnchantments(field, item);
	}
	public void SaveItem(String field, ItemStack item) {
		config.set(field + "Size", item.getAmount());
		config.set(field + "ItemTypeID", item.getTypeId());
		List<String> Enchants = new ArrayList<String>();
		for (Entry<Enchantment, Integer> enchant  : item.getEnchantments().entrySet())
		{
			Enchants.add(enchant.getKey().getId() + ":"+enchant.getValue());
		}
		if (Enchants.size() > 0)
			config.set(field + "Enchantments", Enchants);
	}
	public ItemStack ReadEnchantments(String field, ItemStack _item) {
		String header = field + "Enchantments";
		
		ItemStack item = _item;
		if (config.contains(header)) {
			List<String> enchants = config.getStringList(header);
			for(String str : enchants) {
				String[] info = str.split(":");
				if (info.length == 2) {
					Integer ID = Integer.parseInt(info[0]);
					Integer Level = Integer.parseInt(info[1]);
					if (Level == 0) continue;
					
					item.addEnchantment(Enchantment.getById(ID), Level);
				}
			}
		}
		try { item.addEnchantment(Enchantment.DURABILITY, 3); } catch(Exception e) { }
		return item;
	}
	public void UpdateConfig(String field, Object value) {
		config = new YamlConfiguration();
		try { config.load(path); } catch (IOException | InvalidConfigurationException e) { e.printStackTrace(); return; }
		
		config.set(field, value);
		
		try { config.save(path); } catch (IOException e) { e.printStackTrace(); return; }
	}
}
