package dread9nought.GunGame.ConfigManager;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.GameEngine.GameVariables;

public class MapConfig 
{
	public MapConfig() { Load(); }
	
	YamlConfiguration config;
	String mainPath = GunGame.getPlugin().getDataFolder() + "/";
	String path = mainPath + "mapConfig.yml";
	
	public void Load()
	{
		if (!new File(mainPath).exists()) { new File(mainPath).mkdir(); }
		File file = new File(path);
		if (!file.exists()) { try { file.createNewFile(); } catch(IOException e) { e.printStackTrace(); return; } }
		
		config = new YamlConfiguration();
		try { config.load(path); } catch (IOException | InvalidConfigurationException e) { e.printStackTrace(); return; }
		
		if (!config.contains("map-name")) {
			config.set("map-name", "Arrggg!");
		}
		else
			GameVariables.getMap().setName(config.getString("map-name"));
		
		if (!config.contains("map-lobby")) {
			config.set("map-lobby", "world:0:70:0:0:0");
		}
		else
			GameVariables.getMap().setLobby(getConfigLocation(config.getString("map-lobby")));
		
		if (!config.contains("map-spawnpoints")) {
			config.set("map-spawnpoints", new String[] { "" });
		}
		else
		{
			List<String> spawnpoints = config.getStringList("map-spawnpoints");
			if (spawnpoints.size() > 0)
			{
				for(String s : spawnpoints)
				{
					if (s != "")
					{
						GameVariables.getMap().addSpawn(getConfigLocation(s));
					}
				}
			}
		}
	}
	public Location getConfigLocation(String configLoc) {
		
		try {
			String[] configLocation = configLoc.split(":");
			
			World world = GunGame.getPlugin().getServer().getWorld(configLocation[0]);
			if (world == null) { GunGame.getPlugin().getLogger().severe("Unable to load world: "+configLocation[0]); }
			double x = Double.parseDouble(configLocation[1]);
			double y = Double.parseDouble(configLocation[2]);
			double z = Double.parseDouble(configLocation[3]);
			float pitch = Float.parseFloat(configLocation[4]);
			float yaw = Float.parseFloat(configLocation[5]);
			
			Location loc = new Location(world, x, y, z);
			loc.setPitch(pitch);
			loc.setYaw(yaw);
			
			return loc;
		}
		catch(Exception e) { return null; }
	}
}
