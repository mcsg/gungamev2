package dread9nought.GunGame.ConfigManager;

import org.bukkit.configuration.file.FileConfiguration;

import dread9nought.GunGame.GunGame;
import dread9nought.GunGame.GameEngine.GameVariables;

public class Config
{
	FileConfiguration config;
	public Config() { Load(); }
	
	public void Load()
	{
		config = GunGame.getPlugin().getConfig();
		if (!config.contains("hub-server"))
			config.set("hub-server", "gungame");
		else
			GunGame.getPlugin().getLilypad().hub = config.getString("hub-server");
		
		if (!config.contains("max-players"))
			config.set("max=players", GameVariables.getMaxPlayers());
		else
			GameVariables.setMaxPlayers(config.getInt("max-players"));
		
		if (!config.contains("min-players"))
			config.set("min-players", GameVariables.getMinPlayers());
		else
			GameVariables.setMinPlayers(config.getInt("min-players"));
		
		if (!config.contains("lobby-time"))
			config.set("lobby-time", GameVariables.getLobbyTime());
		else
			GameVariables.setLobbyTime(config.getInt("lobby-time"));
		
		if (!config.contains("lobby-shorttime"))
			config.set("lobby-shorttime", GameVariables.getLobbyShortTime());
		else
			GameVariables.setLobbyShortTime(config.getInt("lobby-shorttime"));
		
		if (!config.contains("game-time"))
			config.set("game-time", GameVariables.getGameTime());
		else
			GameVariables.setGameTime(config.getInt("game-time"));
		GunGame.getPlugin().saveConfig();
	}
}
